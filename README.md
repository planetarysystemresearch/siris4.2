# README #

### SIRIS v4.2 ###

* Code for geometrical optics with radiative transfer
* Version 4.2
	* Mantle-core geometry for the host particle
	* Inhomogeneous waves in absorbing media

### Compiling ###

* There is a Makefile in the 'src'-directory
	* Comment/uncomment options from the Makefile
	* run 'make'
	* Tested on gcc gfortran 8.2, should work on previous versions and with intel ifort, at least

### Super-short usage guide ###

* Check the sample input file in directory 'test'
* run with 'siris42 [inputfile-name] \[[diffuse-scattering-matrix-for-mantle]\] \[[diffuse-scattering-matrix-for-core]\]'

### Contact ###

* Administrator for this repository: Antti Penttil� (antti.i.penttila at helsinki.fi)
* Developers of the SIRIS code: Karri Muinonen, Julia Martikainen, Hannakaisa Lindqvist, Timo V�is�nen, Antti Penttil�
